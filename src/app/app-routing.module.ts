import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AboutComponent } from './page/about/about.component';
import { ConcertComponent } from './page/concert/concert.component';
import { MemberComponent } from './page/member/member.component';
import { SongComponent } from './page/song/song.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'layout', pathMatch: 'full'
  },
  {
    path: 'layout',
    component: LayoutComponent,
    children: [
      {
        path: 'about', component: AboutComponent
      },
      {
        path: 'concert', component: ConcertComponent
      },
      {
        path: 'member', component: MemberComponent
      },
      {
        path: 'song', component: SongComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

