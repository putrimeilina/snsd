import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {

  nestedTableData = [{
    name: " Kim Taeyeon",
    dob: "9 Maret 1989",
    height: 162,
    weight: 44,
    pob: "Jeonju, Korea Selatan",
    hobby: "Berenang",
    image: "../assets/taeyeon.jpg"
  },
  {
    name: "Lee Sunkyu",
    dob: "15 Mei 1989",
    height: 158,
    weight: 43,
    pob: "United States",
    hobby: "Renang, main video game, olahraga",
    image: "../assets/sunny.jpg"
  },
  {
    name: "Stephanie Hwang Mi Young",
    dob: "1 Agustus 1989",
    height: 162,
    weight: 50,
    pob: "Los Angles, America",
    hobby: "Main flute (Seruling)",
    image: "../assets/tiffany.jpg"
  },
  {
    name: "Kim Hyoyeon",
    dob: "22 September 1989",
    height: 160,
    weight: 48,
    pob: "Incheon, Korea Selatan",
    hobby: "Menari (Nge-dance)",
    image: "../assets/hyoyeon.jpg"
  },
  {
    name: "Kwon Yuri",
    dob: "5 Desember 1989",
    height: 167,
    weight: 53,
    pob: "Gyeonggi Province, Korea Selatan",
    hobby: "Dancing, ballet, main piano, violin",
    image: "../assets/yuri.jpg"
  },
  {
    name: "Choi Sooyoung",
    dob: "10 Februari 1990",
    height: 170,
    weight: 50,
    pob: "Gwang City, Korea Selatan",
    hobby: "Makan",
    image: "../assets/sooyoungg.jpg"
  },
  {
    name: "Lim Yoon-Ah",
    dob: "30 Mei 1990",
    height: 166,
    weight: 48,
    pob: "Seoul, Korea Selata",
    hobby: "Makan",
    image: "../assets/yoona.jpg"
  },
  {
    name: "Seo Joo Hyun",
    dob: "29 Juni 1991",
    height: 168,
    weight: 48,
    pob: "Seoul, Korea Selatan",
    hobby: "Main piano, nonton Keroro",
    image: "../assets/seohyun.jpg"

  }];

  constructor() { }

  ngOnInit(): void {

  }

}
