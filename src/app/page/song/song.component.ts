import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.scss']
})
export class SongComponent implements OnInit {

  nestedTableData = [{
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"
  },
  {
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"
  },
  {
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"
  },
  {
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"
  },
  {
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"
  },
  {
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"
  },
  {
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"
  },
  {
    title: " Kim Taeyeon",
    writer: "9 Maret 1989",
    album: 162,
    year: 44,
    image: "../assets/seohyun.jpg"

  }];

  constructor() { }

  ngOnInit(): void {

  }

}
